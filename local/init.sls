local:
  tld: alias.services
  contact_email: ops@alias.services
  internal_eth_adapter: eth1
  public_ips:
    ## This is temporary until we manage automatically Web servers IPs
    noc: 10.10.0.2
    http_origin: 10.10.0.3
  endpoints:
    ## This should be the IP/hostname of a MySQL compatible database server
    mysql: 10.10.0.4

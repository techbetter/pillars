formulas:
  /srv/formulas/basesystem:
    branch: master
    origin: https://github.com/renoirb/salt-basesystem.git
    remotes:
      upstream: git@azure.alias.services:salt-basesystem.git

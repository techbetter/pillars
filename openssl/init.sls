openssl:
  ca_secret: CertificateAuthoritySecret
  intermediate_ca_secret: IntermediateCertificateAuthority
  key_secret: Somethin
  contact_details:
    organization: Alias Web Services
    organization_unit: Certificate Authority
    contact_email: hostmaster@alias.services

# Infrastructure configuration code settings

Using [Salt Stack](http://saltstack.com/).

Can be either used within an **Ubuntu 14.04 LTS** VM with hostname "**noc**" running on a cloud-provider
([Amazon AWS](https://aws.amazon.com/), [Microsoft Azure](https://azure.microsoft.com), [Google GCP](https://cloud.google.com), etc.),
or within a local [Vagrant](https://www.vagrantup.com/) cluster.

In order to use this configuration project, you need a NOC ("Network Operations Controller").
The NOC’s purpose is to keep VMs in a cluster organized.
A deployment level (e.g. UAT, Staging, etc.) should contain only one NOC, and VMs around it should only consider its assigned NOC as source of truth.

To build your own NOC, refer to [this repository](https://bitbucket.org/AliasWebServices/noc),
and initalize a VM using one of the scripts in **ops/provision/** folder.


## How to use

This repository should contain settings that are common accross all clusters.
It should contain settings such as software versions,
external dependencies **[salt-states](https://bitbucket.org/AliasWebServices/salt-states)** will rely upon.

For private data, refer to **[pillars-all](https://bitbucket.org/AliasWebServices/pillars-all)**,
in production, we'll use **[pillars-production](https://bitbucket.org/AliasWebServices/pillars-production)**.
Both repositories should have exact same struture,
except for the fact that **[pillars-production](https://bitbucket.org/AliasWebServices/pillars-production)**
will contain sensitive credentials for production.


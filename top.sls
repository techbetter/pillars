base:

  '*':
    - local

  ## For web developers ... (see note in states top.sls for rationale)
  ## We want all nodes to see source-controlled list of projects,
  ## but Web Developers will have their own local projects pillar.
  '* and not G@id:web-dev':
    - match: compound
    - projects
    - mysql
    - mysql.projects

  ## For Web Developers ...
  ## We want to have TLS certificates but we don't really need to care about
  ## storing them.
  ## It's for development and we just want certs.
  'G@id:web-dev':
    - match: compound
    - openssl

  'noc*':
    - gdnsd
    - formulas
    - openssl
